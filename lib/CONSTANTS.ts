export const SUBJECT = {
	PUBLIC: 1,
	ADMIN: 2,
};

export const AUDIENCE = {
	ACCESS_CMS: '1',
};

export const COOKIES_KEY = {
	USER_TOKEN: '__ut',
	USER_REFRESH_TOKEN: '__urt',
};
