declare namespace User {
	interface Profile {
		id: number;
		fullName: string;
		role: number;
	}
}
